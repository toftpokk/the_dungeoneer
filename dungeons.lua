local modpath = the_dungeoneer.modpath
local function gen_seed()
	return math.random(1,1000)
end

local function register_decoration(dict)
	minetest.register_decoration(dict)
end

local function register_decoration_default(path)
	register_decoration({
		deco_type = "schematic",
		schematic = path,
		place_on = {
			"default:stone",
			"default:desert_stone",
			"default:sandstone",
			"default:desert_sandstone",
			"default:silver_sandstone"
		},
		sidelen = 16,
		noise_params = {
			offset = 0,
			scale = 0.01, -- higher = more decor
			spread = {x=100, y=100,z=100}, -- how large of an area
			seed = gen_seed(), -- random seed base for noise
			octaves = 3, -- noise octave?
			persist = 0.6
		},
		y_min = -3000, -- bottom limit
		y_max = 150, -- height limit
		-- biomes = {"desert"},
		flags = "place_center_x, place_center_z, force_placement",
		rotation = "random",
	})
end

register_decoration({
	deco_type = "schematic",
	schematic = path,
	place_on = {
		"default:stone",
		"default:desert_stone",
		"default:sandstone",
		"default:desert_sandstone",
		"default:silver_sandstone"
	},
	sidelen = 16,
	noise_params = {
		offset = 0,
		scale = 0.01, -- higher = more decor
		spread = {x=100, y=100,z=100}, -- how large of an area
		seed = gen_seed(), -- random seed base for noise
		octaves = 3, -- noise octave?
		persist = 0.6
	},
	y_min = -3000, -- bottom limit
	y_max = 150, -- height limit
	-- biomes = {"desert"},
	flags = "place_center_x, place_center_z, force_placement",
	rotation = "random",
})

register_decoration_default(modpath .. "/schematics/test_farm.mts")
register_decoration_default(modpath .. "/schematics/test_cage.mts")
register_decoration_default(modpath .. "/schematics/test_shack.mts")
register_decoration_default(modpath .. "/schematics/test_statue.mts")
