
minetest.register_node("the_dungeoneer:superirom_ore", {
	description = "The Superior superirom",
	drawtype = "normal",
	is_ground_content = false,
	tiles = {"superirom_ore.png"},
	drop = "default:stone",
	paramtype = "light",
	groups = {cracky=30},
	sunlight_propagates = false,
	sounds = default.node_sound_stone_defaults(),
})
