--[[

The Dungeoneeer by toftpokk & tokkpython

]]

-- epilyx = rawget(_G, "epilyx") or {}
-- math.randomseed(os.time())
the_dungeoneer = {}
the_dungeoneer.modpath = minetest.get_modpath("the_dungeoneer")

-- debug_tools
function DEBUG(text)
	print("DEBUG: "..tostring(text))
	minetest.chat_send_all(tostring(text))
end
--
--
dofile(the_dungeoneer.modpath .. "/dungeons.lua") -- Dungeons library
dofile(the_dungeoneer.modpath .. "/blocks.lua") -- Dungeons library

minetest.register_chatcommand("foo", {
    privs = {
        interact = true,
    },
    func = function(name, param)
        return true, "You said " .. param .. "!"
    end,
})

-- Get content IDs during load time, and store into a local
local c_dirt  = minetest.get_content_id("default:dirt")
local c_grass = minetest.get_content_id("default:dirt_with_grass")

local function grass_to_dirt(pos1, pos2)
    -- Read data into LVM
    local vm = minetest.get_voxel_manip()
    local emin, emax = vm:read_from_map(pos1, pos2)
    local a = VoxelArea:new{
        MinEdge = emin,
        MaxEdge = emax
    }
    local data = vm:get_data()

    -- Modify data
    for z = pos1.z, pos2.z do
        for y = pos1.y, pos2.y do
            for x = pos1.x, pos2.x do
                local vi = a:index(x, y, z)
                if data[vi] == c_grass then
                    data[vi] = c_dirt
                end
            end
        end
    end

    -- Write data
    vm:set_data(data)
    vm:write_to_map(true)
end
